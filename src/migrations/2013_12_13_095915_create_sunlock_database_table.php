<?php

use Illuminate\Database\Migrations\Migration;

class CreateSunlockDatabaseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(Config::get('sunlock::lock.table'), function($t)
		{
			$t->string('lockid', 50);
			$t->primary('lockid');
			$t->enum('state', array('ro', 'rw'));
			$t->string('payload', 50);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop(Config::get('sunlock::lock.table'));
	}

}