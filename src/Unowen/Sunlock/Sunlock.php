<?php namespace Unowen\Sunlock;

use Illuminate\Support\Facades\Facade;

class Sunlock extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'sunlock'; }

}