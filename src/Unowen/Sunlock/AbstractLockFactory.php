<?php

namespace Unowen\Sunlock;

abstract class AbstractLockFactory
{
	private $lockName;
	private $ignoreLockAfter;
	private $isLocked = false;
	private $errorMsg;

	abstract protected function normalizeLockName($lockname);

	public final function tryLock()
	{
		if (true == $this->isLocked)
			return $this->error('already locked');

		$selfTime = time();
		$selfId = uniqid() . '.' . $selfTime;

		try {
			$lockId = $this->lockSaveLoad($this->lockName, $selfId);
			if ($lockId === $selfId) {
				$this->isLocked = true;
			} elseif ($this->ignoreLockAfter != 0) {
				// retry on lock existing for more than specified timeout
				$isLockStale = false;
				if (strpos($lockId, '.') === 13) {
					list(, $lockTime) = explode('.', $lockId);
					$isLockStale = ($selfTime - $lockTime) > $this->ignoreLockAfter;
				} else {
					$isLockStale = true; // old or unspported format
				}
				if ($isLockStale) {
					$this->deleteExistingLock($this->lockName);
					$lockId = $this->lockSaveLoad($this->lockName, $selfId);
					if ($lockId === $selfId) {
						$this->isLocked = true;
					}
				}
			}
		} catch (SunLockOperationException $e) {
			return $this->error($e->getMessage());
		}

		if ($this->isLocked) {
			register_shutdown_function(array($this, '_emergencyBackupCleanup')); // rewrite as lambda when upgraded
			return $this->success();
		}

		return $this->error('failed to lock');
	}

	abstract protected function lockSaveLoad($lockname, $contents);

	function _emergencyBackupCleanup() // can't make this private beacause of how r_s_f works
	{
		if (false == $this->isLocked)
			return;

		$this->deleteExistingLock($this->lockName);
	}

	public final function unlock()
	{
		if (false == $this->isLocked)
			return $this->error('not locked');

		try {
			$this->deleteExistingLock($this->lockName);
			$this->isLocked = false;
			return $this->success();
		} catch (SunLockOperationException $e) {
			$this->error('failed to delete lockfile');
		}
	}

	abstract protected function deleteExistingLock($lockname);

	private function error($msg)
	{
		$this->errorMsg = $msg;
		return false;
	}

	private function success()
	{
		return true;
	}
}

class SunLockOperationException extends \Exception{}
