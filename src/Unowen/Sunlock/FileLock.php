<?php

namespace Unowen\Sunlock;

class FileLock extends AbstractLock
{
	private $lockfile;

	function __construct($path, $name, $timeout)
	{
		$this->lockfile = $this->normalizeLockName($path . DIRECTORY_SEPARATOR . $name . '.lock');
		parent::__construct($timeout);
	}

	private function normalizeLockName($filename)
	{
		return realpath(dirname($filename)) . DIRECTORY_SEPARATOR . basename($filename);
	}

	protected function lockSaveLoad($lockContents)
	{
		// don't use chmod instead of umask despite documentation notice -- doesn't suit our needs
		$oldUmask = umask();
		umask(0277);
		$fp = @fopen($this->lockfile, 'w'); // umask is not necesserilly 0277 here. Non thread-safe yay!
		umask($oldUmask);
		@chmod($this->lockfile, 0400);// see above (it *must* be readonly), although it might actually be too late already if it didn't work in the first place
		if (false !== $fp) {
			$written = @fwrite($fp, $lockContents);
			fclose($fp);
		}

		if (false === ($fp = @fopen($this->lockfile, 'r')))
			throw new SunlockException('failed to acquire lockfile (ro)');

		$id = @fread($fp, 24);
		fclose($fp);
		if (false === $id)
			return '';

		return $id;
	}

	protected function deleteExistingLock()
	{
		if (is_file($this->lockfile)) {
			if (false == @unlink($this->lockfile)) {
				// Virtualbox VM bug?
				@chmod($this->lockfile, 0600);
				if (false == @unlink($this->lockfile)) {
					throw new SunlockException('failed to delete existing lockfile');
				}
			}
		}
	}
}