<?php namespace Unowen\Sunlock;

use \Closure;

class Repository {

	/**
	 * The lock implementation builder.
	 *
	 * @var Closure
	 */
	protected $lock;

	/**
	 * Create a new lock repository instance.
	 *
	 * @param  Closure $lock
	 */
	public function __construct(Closure $lock)
	{
		$this->lock = $lock;
	}

	/**
	 * Return a usable lock instance
	 *
	 * @param string $key
	 * @param int $timeout
	 * @return \Unowen\Sunlock\AbstractLock child of
	 */
	public function make($key, $timeout)
	{
		return $this->lock->__invoke($key, $timeout);
	}
}
