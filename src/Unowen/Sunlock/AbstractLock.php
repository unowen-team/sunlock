<?php

namespace Unowen\Sunlock;

abstract class AbstractLock
{
	protected $ignoreLockAfter;
	private $isLocked = false;

	function __construct($ignoreLockAfter)
	{
		$this->ignoreLockAfter = intval($ignoreLockAfter);
	}

	public final function lock()
	{
		if (true == $this->isLocked)
			throw new SunlockException('already locked');

		$selfTime = time();
		$selfId = uniqid() . '.' . $selfTime;

		$lockId = $this->lockSaveLoad($selfId);
		if ($lockId === $selfId) {
			$this->isLocked = true;
		} elseif ($this->ignoreLockAfter != 0) {
			// retry on lock existing for more than specified timeout
			$isLockStale = false;
			if (strpos($lockId, '.') === 13) {
				list(, $lockTime) = explode('.', $lockId);
				$isLockStale = ($selfTime - $lockTime) > $this->ignoreLockAfter;
			} else {
				$isLockStale = true; // old or unspported format
			}
			if ($isLockStale) {
				$this->deleteExistingLock();
				$lockId = $this->lockSaveLoad($selfId);
				if ($lockId === $selfId) {
					$this->isLocked = true;
				}
			}
		}

		if (!$this->isLocked)
			throw new SunlockException('failed to lock');

		register_shutdown_function(array($this, '_emergencyBackupCleanup')); // rewrite as lambda when upgraded
	}

	abstract protected function lockSaveLoad($contents);

	function _emergencyBackupCleanup() // can't make this private beacause of how r_s_f works
	{
		if (false == $this->isLocked)
			return;

		$this->deleteExistingLock();
	}

	public final function unlock()
	{
		if (false == $this->isLocked)
			throw new SunlockException('not locked');

		$this->deleteExistingLock();
		$this->isLocked = false;
	}

	abstract protected function deleteExistingLock();
}

class SunlockException extends \Exception{}
