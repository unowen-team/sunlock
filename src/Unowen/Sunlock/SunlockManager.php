<?php namespace Unowen\Sunlock;

use \Illuminate\Support\Manager;
use \Closure;

class SunlockManager extends Manager {

	/**
	 * Create an instance of the file lock driver.
	 *
	 * @return \Unowen\Sunlock\FileLock
	 */
	protected function createFileDriver()
	{
		$path = $this->app['config']['sunlock::lock.path'];

		return $this->repository(function($name, $timeout) use ($path) {
			return new FileLock($path, $name, $timeout);
		});
	}

	/**
	 * Create an instance of the database lock driver.
	 *
	 * @return \Unowen\Sunlock\DatabaseLock
	 */
	protected function createDatabaseDriver()
	{
		$connection = $this->getDatabaseConnection();

		$table = $this->app['config']['sunlock::lock.table'];

		return $this->repository(function($name, $timeout) use ($connection, $table) {
			return new DatabaseLock($connection, $table, $name, $timeout);
		});
	}

	/**
	 * Get the database connection for the database driver.
	 *
	 * @return \Illuminate\Database\Connection
	 */
	protected function getDatabaseConnection()
	{
		$connection = $this->app['config']['sunlock::lock.connection'];

		return $this->app['db']->connection($connection);
	}

	/**
	 * Create a new lock builder repository with the given implementation.
	 *
	 * @param  \Closure  $store
	 * @return \Unowen\Sunlock\Repository
	 */
	protected function repository(Closure $store)
	{
		return new Repository($store);
	}

	/**
	 * Get the default lock driver name.
	 *
	 * @return string
	 */
	protected function getDefaultDriver()
	{
		return $this->app['config']['sunlock::lock.driver'];
	}

}