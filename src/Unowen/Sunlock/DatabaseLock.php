<?php namespace Unowen\Sunlock;

use Illuminate\Database\Connection;

class DatabaseLock extends AbstractLock
{
	private $connection;
	private $lockname;
	private $table;

	function __construct(Connection $connection, $table, $name, $timeout)
	{
		$this->connection = $connection;
		$this->table = $table;
		$this->lockname = $name;
		parent::__construct($timeout);
	}

	protected function lockSaveLoad($lockContents)
	{
		// $this->connection->ping();
		$table = $this->connection->getQueryGrammar()->wrapTable($this->table);

		try {
			$this->connection->insert(
				'INSERT IGNORE INTO ' . $table .
				' (lockid, state)' .
				' VALUES(?, "rw")',
			array(
				$this->lockname
			));

			$this->connection->update(
				'UPDATE ' . $table .
				'SET payload=?, state="ro"' .
				'WHERE lockid=? AND state="rw"',
			array(
				$lockContents, $this->lockname
			));

			$payload = $this->connection->table($this->table)
				->where('lockid', $this->lockname)
				->lists('payload');
			// if (!isset($))

			if (0 == count($payload))
				return '';

			return $payload[0];
		} catch (\Exception $e) {
			throw new SunlockException($e);
		}
	}

	protected function deleteExistingLock()
	{
		// $this->connection->ping();
		$table = $this->connection->getQueryGrammar()->wrapTable($this->table);

		$this->connection->delete(
			'DELETE FROM ' . $table .
			'WHERE lockid=?',
		array(
			$this->lockname
		));
	}
}
