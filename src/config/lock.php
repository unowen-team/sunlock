<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default Lock Driver
	|--------------------------------------------------------------------------
	|
	| This option controls the default lock "driver" that will be used when
	| using the Lock library. This is the default when another is not specified.
	|
	| Supported: "file", "database"
	|
	*/

	'driver' => 'database',

	/*
	|--------------------------------------------------------------------------
	| File Lock Location
	|--------------------------------------------------------------------------
	|
	| Changeable default
	|
	*/

	'path' => storage_path().'/locks',

	/*
	|--------------------------------------------------------------------------
	| Database Lock Connection
	|--------------------------------------------------------------------------
	|
	| When using the "database" lock driver you may specify the connection
	| that should be used to store the locks. When this option is
	| null the default database connection will be utilized for locking.
	|
	*/

	'connection' => null,

	/*
	|--------------------------------------------------------------------------
	| Database Locks Table
	|--------------------------------------------------------------------------
	|
	| Changeable default
	|
	*/

	'table' => 'locks',
);
